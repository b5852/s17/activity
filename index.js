// console.log("Hello World");


let name = [];

// Create an addStudent() function that will accept a name of the student and add it to the student array.
function addStudent(x){

	name.push(x);
	console.log(x + " was added to the student's list");

}

// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){

	console.log("The total size of the array is " + name.length);

}

// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){

	name.forEach(function(y){
		console.log(y);
	})
}

/*
	6. Create a findStudent() function that will do the following:
	Search for a student name when a keyword is given (filter method).
	If one match is found print the message studentName is an enrollee.
	If multiple matches are found print the message studentNames are enrollees.
	If no match is found print the message studentName is not an enrollee.
	The keyword given should not be case sensitive.
*/

function findStudent(student){

	let search = name.filter(function(z)
	{
		return z.toLowerCase().includes(student);
	})

	if (search.length === 1) {
		
		console.log(student + " is an enrolee.");

	}else if(search.length > 1){
		console.log(name + " are enrollees.");

	}else{
		console.log("No student found with the name " );

	}
		
}







